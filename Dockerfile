FROM openjdk:14-slim
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} application.jar
ENTRYPOINT ["java", "--enable-preview", "-jar", "/application.jar"]
