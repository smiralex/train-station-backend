package ru.nsu.fit.db.railway.trains.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.nsu.fit.db.railway.abstraction.model.Filter;
import ru.nsu.fit.db.railway.abstraction.model.dto.PageableDto;
import ru.nsu.fit.db.railway.common.dto.TrainStationDto;
import ru.nsu.fit.db.railway.common.mapper.TrainStationMapper;
import ru.nsu.fit.db.railway.common.repositories.SeatCategoriesRepository;
import ru.nsu.fit.db.railway.entities.trains.RailwayTrack;
import ru.nsu.fit.db.railway.entities.trains.Train;
import ru.nsu.fit.db.railway.exceptions.NotFoundException;
import ru.nsu.fit.db.railway.trains.model.TrainFilter;
import ru.nsu.fit.db.railway.trains.repositories.TrainsRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class TrainsService {

    private final TrainsRepository trainsRepository;
    private final SeatCategoriesRepository seatCategoriesRepository;
    private final TrainStationMapper trainStationMapper;

    public List<Integer> getAllTrainIds() {
        return trainsRepository.findAllTrainNumbers();
    }

    public List<String> getAllSeatCategories(){
        return seatCategoriesRepository.findAllNames();
    }

    public Page<Train> getFilteredTrains(Filter<TrainFilter> filter) {
        if (filter.getPageSize() == 0) {
            filter.setPageSize(PageableDto.DEFAULT_PAGE_SIZE);
        }
        return trainsRepository.findByFilter(filter.getFilter(),
                PageRequest.of(filter.getPageNumber(), filter.getPageSize()));

    }

    public List<TrainStationDto> getRoute(int trainId) {
        Train train =  trainsRepository
                .findById(trainId)
                .orElseThrow(() -> new NotFoundException("Train not found"));
        Set<RailwayTrack> railwayTracks = train.getRailwayTracks();
        List<TrainStationDto> route = new ArrayList<>();
        TrainStationDto currentStationDto = trainStationMapper.toDto(train.getDepartureStation());
        route.add(currentStationDto);
        while (!railwayTracks.isEmpty()) {
            Iterator<RailwayTrack> iterator = railwayTracks.iterator();
            while (iterator.hasNext()){
                RailwayTrack railwayTrack = iterator.next();
                if (railwayTrack.getDepartureStation().getId() == currentStationDto.getId()){
                    currentStationDto = trainStationMapper.toDto(railwayTrack.getArrivalStation());
                    route.add(currentStationDto);
                    iterator.remove();
                    break;
                }
            }
        }
        return route;
    }
}
