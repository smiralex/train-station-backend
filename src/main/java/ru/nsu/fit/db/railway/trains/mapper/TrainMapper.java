package ru.nsu.fit.db.railway.trains.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.abstraction.mapper.Mapper;
import ru.nsu.fit.db.railway.entities.trains.Train;
import ru.nsu.fit.db.railway.entities.trains.TrainStation;
import ru.nsu.fit.db.railway.trains.model.dto.TrainDto;

@Component
@RequiredArgsConstructor
public class TrainMapper implements Mapper<Train, TrainDto>  {

    private final ModelMapper modelMapper;

    @Override
    public Train toEntity(TrainDto dto) {
        return modelMapper.map(dto, Train.class);
    }

    @Override
    public TrainDto toDto(Train train) {
        TrainDto dto =  modelMapper.map(train, TrainDto.class);
        TrainStation depStation = train.getDepartureStation();
        TrainStation arrStation = train.getArrivalStation();
        dto.setDepartureCity(depStation.getStationCity());
        dto.setArrivalCity(arrStation.getStationCity());
        dto.setArrivalStationName(depStation.getStationName());
        dto.setDepartureStationName(arrStation.getStationName());
        dto.setTrainCategory(train.getTrainCategory().getCategoryName());
        return dto;
    }
}
