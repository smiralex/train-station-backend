package ru.nsu.fit.db.railway.schedule.services;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import ru.nsu.fit.db.railway.common.repositories.StationsRepository;
import ru.nsu.fit.db.railway.common.repositories.WayPointsRepository;
import ru.nsu.fit.db.railway.entities.trains.Train;
import ru.nsu.fit.db.railway.entities.trains.TrainRide;
import ru.nsu.fit.db.railway.entities.trains.TrainStation;
import ru.nsu.fit.db.railway.entities.trains.WayPoint;
import ru.nsu.fit.db.railway.exceptions.NotFoundException;
import ru.nsu.fit.db.railway.schedule.model.dto.TrainRideDto;
import ru.nsu.fit.db.railway.schedule.model.dto.WayPointDto;
import ru.nsu.fit.db.railway.schedule.model.dto.WayPointEditDto;
import ru.nsu.fit.db.railway.schedule.repositories.TrainRidesRepository;
import ru.nsu.fit.db.railway.trains.repositories.TrainsRepository;

import javax.transaction.Transactional;
import java.util.*;

@Service
@RequiredArgsConstructor
public class TrainRideService {

    private final TrainRidesRepository trainRidesRepository;
    private final WayPointsRepository wayPointsRepository;
    private final TrainsRepository trainsRepository;
    private final StationsRepository stationsRepository;

    public TrainRide getLastTrainRide(int trainId) {
        return trainRidesRepository.findLastTrainRide(trainId);
    }

    @Transactional
    public void editWayPoint(WayPointEditDto wayPointEditDto) {
        TrainRide trainRide = trainRidesRepository.getWithWayPointsById(wayPointEditDto.getRideId());
        if (trainRide == null) {
            throw new NotFoundException("Train ride not found");
        }
        List<WayPoint> wayPoints = new ArrayList<>(trainRide.getWayPoints());
        wayPoints.sort(Comparator.comparing(WayPoint::getArrivalDate));
        Date newDate = wayPointEditDto.getArrivalDate();
        Integer newStayTime = wayPointEditDto.getStayTime();
        Integer newDelay = wayPointEditDto.getDelay();
        WayPoint targetWayPoint = null;
        for (int i = 0; i < wayPoints.size(); i++) {
            WayPoint wp = wayPoints.get(i);
            if (wp.getTrainStation().getId() == wayPointEditDto.getStationId()) {
                targetWayPoint = wp;
                // check edit data is correct
                if (i != wayPoints.size() -1) {
                    WayPoint wpNext = wayPoints.get(i + 1);
                    if (newDate != null && wpNext.getArrivalDate().compareTo(newDate) <= 0) {
                        throw new IllegalArgumentException("Arrival date is too late");
                    }
                    if (newStayTime != null) {
                        Date afterStay = DateUtils.addMinutes(wp.getArrivalDate(), newStayTime);
                        if (wpNext.getArrivalDate().compareTo(afterStay) <= 0) {
                            throw new IllegalArgumentException("Stay time is too big");
                        }
                    }
                    if (newDelay != null && wpNext.getDelay() < newDelay) {
                        throw new IllegalArgumentException("Delay is too big");
                    }
                }
                if (i != 0) {
                    WayPoint wpPrev = wayPoints.get(i - 1);
                    if (newDate != null && wpPrev.getArrivalDate().compareTo(newDate) >= 0) {
                        throw new IllegalArgumentException("Arrival date is too early");
                    }
                    if (newDelay != null && wpPrev.getDelay() > newDelay) {
                        throw new IllegalArgumentException("Delay is too small");
                    }
                }
                //edit
                if (newDate != null) {
                    wp.setArrivalDate(newDate);
                    if (i == 0) {
                        trainRide.setDepartureDate(newDate);
                    }
                    if (i == wayPoints.size() - 1) {
                        trainRide.setArrivalDate(newDate);
                    }
                }
                if (newStayTime != null) {
                    wp.setStayTime(newStayTime);
                }
                if (newDelay != null) {
                    wp.setDelay(newDelay);
                }
            }
        }
        trainRidesRepository.save(trainRide);
        if (targetWayPoint == null) {
            throw new NotFoundException("Way point not found");
        } else {
            wayPointsRepository.save(targetWayPoint);
        }
    }

    @Transactional
    public void createTrainRide(TrainRideDto trainRideDto) {
        List<WayPointDto> wayPointDtos = trainRideDto.getRoute();
        // check waypoints is correct
        for (int i = 0; i < wayPointDtos.size(); i++) {
            WayPointDto wpDto = wayPointDtos.get(i);
            if (wpDto.getArrivalDate() == null) {
                throw new IllegalArgumentException("Arrival date can not be null");
            }
            if (i != wayPointDtos.size() - 1) {
                WayPointDto wpDtoNext = wayPointDtos.get(i + 1);
                if (wpDtoNext.getArrivalDate().compareTo(wpDto.getArrivalDate()) <= 0) {
                    throw new IllegalArgumentException("Arrival date is too late");
                }
                Date afterStay = DateUtils.addMinutes(wpDto.getArrivalDate(), wpDto.getStayTime());
                if (wpDtoNext.getArrivalDate().compareTo(afterStay) <= 0) {
                    throw new IllegalArgumentException("Stay time is too big");
                }
                if (wpDtoNext.getDelay() < wpDto.getDelay()) {
                    throw new IllegalArgumentException("Delay is too big");
                }
            }
        }
        Train train = trainsRepository.findById(trainRideDto.getTrainId())
                .orElseThrow(() -> new NotFoundException("Train with id " + trainRideDto.getId() + " not found"));
        TrainRide trainRide = new TrainRide();
        trainRide.setTrain(train);
        TrainRide lastRide = getLastTrainRide(train.getId());
        Set<WayPoint> wayPoints = new HashSet<>();
        trainRideDto.getRoute().forEach(trDto -> {
            TrainStation station = stationsRepository.findById(trDto.getStationId())
                    .orElseThrow(() -> new NotFoundException("Station with id " + trDto.getStationId() + " not found"));
            if (lastRide != null) {
                lastRide.getWayPoints().forEach(ride -> {
                    if (ride.getArrivalDate().compareTo(trDto.getArrivalDate()) >= 0) {
                        throw new IllegalArgumentException("Station on " + station.getStationCity() + " has to early arrival date");
                    }
                });
            }
            WayPoint wayPoint = new WayPoint();
            wayPoint.setArrivalDate(trDto.getArrivalDate());
            wayPoint.setStayTime(trDto.getStayTime());
            wayPoint.setDelay(trDto.getDelay());
            wayPoint.setTrainStation(station);
            wayPoint.setTrainRide(trainRide);
            wayPoints.add(wayPoint);
        });
        trainRide.setDepartureDate(wayPointDtos.get(0).getArrivalDate());
        trainRide.setArrivalDate(wayPointDtos.get(wayPointDtos.size() - 1).getArrivalDate());
        trainRidesRepository.save(trainRide);
        wayPointsRepository.saveAll(wayPoints);
        wayPointsRepository.flush();
    }

}

