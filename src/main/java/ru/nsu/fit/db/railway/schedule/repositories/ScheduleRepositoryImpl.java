package ru.nsu.fit.db.railway.schedule.repositories;

import org.springframework.stereotype.Repository;
import ru.nsu.fit.db.railway.exceptions.WrongFilterException;
import ru.nsu.fit.db.railway.schedule.model.ScheduleFilter;
import ru.nsu.fit.db.railway.schedule.model.ScheduleRow;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ScheduleRepositoryImpl implements ScheduleRepository{

    private static final String TRACKS = """
    WITH RECURSIVE TRACKS (DepStId, DepStCity, DepStName, DepDate, DepDelay, DepStayTime,
                           ArrStId, ArrStCity, ArrStName, ArrDate, ArrDelay, ArrStayTime,
                           TrainDepStCity, TrainArrStCity, TrainNumber, RideId, RideDepDate, RideArrDate,
                           NumSeats, NumPurchasedSeats, SeatCategory) AS (
        SELECT DISTINCT TS_DEP.Id, TS_DEP.StationCity, TS_DEP.StationName, WP_DEP.ArrivalDate, WP_DEP.Delay, WP_DEP.StayTime,
                        TS_ARR.Id, TS_ARR.StationCity, TS_ARR.StationName, WP_ARR.ArrivalDate, WP_ARR.Delay, WP_DEP.StayTime,
                        TS_TRAIN_DEP.StationCity, TS_TRAIN_ARR.StationCity, TrainNumber, TrainRides.Id, 
                        TrainRides.DepartureDate, TrainRides.ArrivalDate, NumSeats,
                        (SELECT COUNT(*) FROM TicketsPurchases WHERE TicketId = Tickets.Id), CategoryName
        FROM RailwayTracks
                 JOIN TrainStations TS_DEP ON RailwayTracks.DepartureStationId = TS_DEP.Id
                 JOIN TrainStations TS_ARR ON RailwayTracks.ArrivalStationId = TS_ARR.Id
                 JOIN WayPoints WP_DEP ON TS_DEP.Id = WP_DEP.StationId
                 JOIN WayPoints WP_ARR ON TS_ARR.Id = WP_ARR.StationId
                 JOIN RouteEdges ON RailwayTracks.Id = RouteEdges.RailwayTrackId
                 JOIN Trains ON RouteEdges.TrainId = Trains.Id
                 JOIN TrainStations TS_TRAIN_DEP ON TS_TRAIN_DEP.Id = Trains.DepartureStationId
                 JOIN TrainStations TS_TRAIN_ARR ON TS_TRAIN_ARR.Id = Trains.ArrivalStationId
                 JOIN TrainSeatCategories ON Trains.Id = TrainSeatCategories.TrainId
                 JOIN SeatCategories ON TrainSeatCategories.SeatCategoryId = SeatCategories.Id
                 LEFT JOIN Tickets ON RailwayTracks.Id = Tickets.RailwayTrackId
                    AND TrainSeatCategories.Id = Tickets.TrainSeatCategoryId
                 JOIN TrainRides ON WP_DEP.RideId = TrainRides.Id
        WHERE (WP_DEP.Delay >= :minDelay)
          AND (WP_DEP.ArrivalDate < WP_ARR.ArrivalDate)
          AND (WP_DEP.RideId = WP_ARR.RideId)
            """;

    private static final String R = """
    ),
                   R AS (
                       SELECT DISTINCT DepStId, DepStCity, DepStName, DepDate, DepDelay, DepStayTime,
                                       ArrStId, ArrStCity, ArrStName, ArrDate, ArrDelay, ArrStayTime,
                                       TrainDepStCity, TrainArrStCity, TrainNumber, RideId, 
                                       RideDepDate, RideArrDate, NumSeats,
                                       NumPurchasedSeats, SeatCategory, ARRAY[TRACKS.ArrStId] AS PassedStations
                       FROM TRACKS
                       WHERE (TRACKS.DepStCity = :cityFrom)
                       
                       UNION
    
                       SELECT DISTINCT TRACKS.DepStId, TRACKS.DepStCity, TRACKS.DepStName, TRACKS.DepDate, TRACKS.DepDelay, TRACKS.DepStayTime,
                                       TRACKS.ArrStId, TRACKS.ArrStCity, TRACKS.ArrStName, TRACKS.ArrDate, TRACKS.ArrDelay, TRACKS.ArrStayTime,
                                       TRACKS.TrainDepStCity, TRACKS.TrainArrStCity, TRACKS.TrainNumber, 
                                       TRACKS.RideId, TRACKS.RideDepDate, TRACKS.RideArrDate, TRACKS.NumSeats,
                                       TRACKS.NumPurchasedSeats, TRACKS.SeatCategory, PassedStations || TRACKS.ArrStId
                       FROM R JOIN TRACKS
                                   ON R.ArrStId = TRACKS.DepStId
                       WHERE (R.DepStId != TRACKS.ArrStId)
                         AND (TRACKS.DepStCity != :cityTo)
           """;

    private static final String UNION = """
                   )
    SELECT DISTINCT DepStName, DepStCity, DepStId, TO_CHAR(DepDate, 'DD-MM-YYYY HH24:MI:SS') AS DepDate, DepDelay, DepStayTime,
                    ArrStName, ArrStCity, ArrStId, TO_CHAR(ArrDate, 'DD-MM-YYYY HH24:MI:SS') AS ArrDate, ArrDelay, ArrStayTime,
                    TrainDepStCity, TrainArrStCity, TrainNumber,  RideId, 
                    TO_CHAR(RideDepDate, 'DD-MM-YYYY HH24:MI:SS') AS RideDepDate, 
                    TO_CHAR(RideArrDate, 'DD-MM-YYYY HH24:MI:SS') AS RideArrDate,
                    NumSeats, NumPurchasedSeats, SeatCategory, PassedStations
    FROM R
    WHERE (NumSeats - NumPurchasedSeats >= :numSeats)
    AND ArrStId = ANY(coalesce((SELECT PassedStations FROM R WHERE R.ArrStCity = :cityTo limit 1)))
            """;

    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public List<ScheduleRow> findSchedule(ScheduleFilter filter) {
        if (filter.getCityFrom() == null || filter.getCityTo() == null) {
            throw new WrongFilterException("CityFrom and CityTo must be specified");
        }
        StringBuilder queryBuilder = new StringBuilder(TRACKS);
        if (filter.getDateFrom() != null) {
            queryBuilder.append("\nAND (WP_DEP.ArrivalDate  >= :dateFrom)");
        }
        if (filter.getDateTo() != null) {
            queryBuilder.append("\nAND (WP_ARR.ArrivalDate - INTERVAL '1 day' < :dateTo) ");
        }
        if (filter.getMaxDelay() != -1) {
            queryBuilder.append("\nAND (WP_ARR.Delay <= :maxDelay)");
        }
        if (filter.getTrainNumbers() != null && !filter.getTrainNumbers().isEmpty()) {
            queryBuilder.append("\nAND (TrainNumber IN (:trainNumbers))");
        }
        if (filter.getSeatTypes() != null && !filter.getSeatTypes().isEmpty()) {
            queryBuilder.append("\nAND (CategoryName IN (:seatTypes))");
        }
        queryBuilder.append(R);
        queryBuilder.append(UNION);
        Query query = em.createNativeQuery(queryBuilder.toString(), "ScheduleRowMapping")
                .setParameter("cityFrom", filter.getCityFrom())
                .setParameter("cityTo", filter.getCityTo())
                .setParameter("minDelay", filter.getMinDelay())
                .setParameter("numSeats", filter.getNumSeats());

        if (filter.getDateFrom() != null) {
            query.setParameter("dateFrom", filter.getDateFrom(), TemporalType.TIMESTAMP);
        }
        if (filter.getDateTo() != null) {
            query.setParameter("dateTo", filter.getDateTo(), TemporalType.TIMESTAMP);
        }
        if (filter.getMaxDelay() != -1) {
            query.setParameter("maxDelay", filter.getMaxDelay());
        }
        if (filter.getTrainNumbers() != null && !filter.getTrainNumbers().isEmpty()) {
            query.setParameter("trainNumbers", filter.getTrainNumbers());
        }
        if (filter.getSeatTypes() != null && !filter.getSeatTypes().isEmpty()) {
            query.setParameter("seatTypes", filter.getSeatTypes());
        }
        return query.getResultList();
    }
}