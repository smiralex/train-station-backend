package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;
import ru.nsu.fit.db.railway.schedule.model.ScheduleRow;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "TrainRides")
@SqlResultSetMapping(
        name = "ScheduleRowMapping",
        classes = @ConstructorResult(
                targetClass = ScheduleRow.class,
                columns = {
                        @ColumnResult(name = "depStName"),
                        @ColumnResult(name = "depStCity"),
                        @ColumnResult(name = "depStId", type = Integer.class),
                        @ColumnResult(name = "depDate"),
                        @ColumnResult(name = "depDelay", type = Integer.class),
                        @ColumnResult(name = "depStayTime", type = Integer.class),
                        @ColumnResult(name = "arrStName"),
                        @ColumnResult(name = "arrStCity"),
                        @ColumnResult(name = "arrStId", type = Integer.class),
                        @ColumnResult(name = "arrDate"),
                        @ColumnResult(name = "arrDelay", type = Integer.class),
                        @ColumnResult(name = "arrStayTime", type = Integer.class),
                        @ColumnResult(name = "trainDepStCity"),
                        @ColumnResult(name = "trainArrStCity"),
                        @ColumnResult(name = "trainNumber", type = Integer.class),
                        @ColumnResult(name = "rideId", type = Integer.class),
                        @ColumnResult(name = "rideDepDate"),
                        @ColumnResult(name = "rideArrDate"),
                        @ColumnResult(name = "numSeats", type = Integer.class),
                        @ColumnResult(name = "numPurchasedSeats", type = Integer.class),
                        @ColumnResult(name = "seatCategory")}))
public class TrainRide extends Identifiable {

    @Temporal(TemporalType.TIMESTAMP)
    private Date departureDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date arrivalDate;

    @ManyToOne
    @JoinColumn(name = "TrainId")
    private Train train;

    @ManyToMany
    @JoinTable(
            name = "TrainCrews",
            joinColumns = @JoinColumn(name = "RideId"),
            inverseJoinColumns = @JoinColumn(name = "EmployeeId")
    )
    private Set<TrainEmployee> crew;

    @OneToMany(mappedBy = "trainRide")
    private Set<WayPoint> wayPoints;

    @OneToMany(mappedBy = "trainRide")
    private Set<Ticket> tickets;
}
