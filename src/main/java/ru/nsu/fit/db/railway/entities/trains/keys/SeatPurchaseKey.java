package ru.nsu.fit.db.railway.entities.trains.keys;

import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
public class SeatPurchaseKey implements Serializable {

    private int railwayTrackId;

    private int rideId;

    private int trainSeatCategoryId;

}