package ru.nsu.fit.db.railway.schedule.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class ScheduleRow {

    private String depStName;

    private String depStCity;

    private int depStId;

    private String depDate;

    private int depDelay;

    private int depStayTime;

    private String arrStName;

    private String arrStCity;

    private int arrStId;

    private String arrDate;

    private int arrDelay;

    private int arrStayTime;

    private String trainDepStCity;

    private String trainArrStCity;

    private int trainNumber;

    private int rideId;

    private String rideDepDate;

    private String rideArrDate;

    private int numSeats;

    private int numPurchasedSeats;

    private String seatCategory;

}
