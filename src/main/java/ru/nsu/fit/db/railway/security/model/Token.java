package ru.nsu.fit.db.railway.security.model;

import lombok.Getter;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;
import ru.nsu.fit.db.railway.entities.User;


import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Tokens")
public class Token extends Identifiable {
    public enum Type {
        REFRESH,
        PASSWORD_RESTORE,
        EMAIL_CONFIRM
    }

    String stringRepresentation;

    @OneToOne
    @JoinColumn(name = "UserId")
    User user;

    @Temporal(TemporalType.TIMESTAMP)
    Date expirationDate;

    @Enumerated(EnumType.STRING)
    Type type;
}