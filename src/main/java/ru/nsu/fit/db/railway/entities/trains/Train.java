package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "Trains")
public class Train extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "TrainCategoryId")
    private TrainCategory trainCategory;

    @ManyToOne
    @JoinColumn(name = "DepartureStationId")
    private TrainStation departureStation;

    @ManyToOne
    @JoinColumn(name = "ArrivalStationId")
    private TrainStation arrivalStation;

    @ManyToOne
    @JoinColumn(name = "HeadStationId")
    private TrainStation headStation;

    private int trainNumber;

    @ManyToMany
    @JoinTable(
            name = "RouteEdges",
            joinColumns = @JoinColumn(name = "TrainId"),
            inverseJoinColumns = @JoinColumn(name = "RailwayTrackId")
    )
    private Set<RailwayTrack> railwayTracks;

    @OneToMany(mappedBy = "train")
    private Set<TrainRide> trainRides;
}
