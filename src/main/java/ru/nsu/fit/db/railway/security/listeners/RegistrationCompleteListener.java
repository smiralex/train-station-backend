package ru.nsu.fit.db.railway.security.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.ApplicationProperties;
import ru.nsu.fit.db.railway.entities.User;
import ru.nsu.fit.db.railway.security.events.RegistrationCompleteEvent;
import ru.nsu.fit.db.railway.security.model.Token;
import ru.nsu.fit.db.railway.security.services.EmailService;
import ru.nsu.fit.db.railway.security.services.TokenService;


@Component
@RequiredArgsConstructor
@Slf4j
public class RegistrationCompleteListener implements ApplicationListener<RegistrationCompleteEvent> {

    private final ApplicationProperties applicationProperties;

    private final EmailService emailService;

    private final TokenService tokenService;

    /**
     * Этот метод вызывается, когда случился {@link RegistrationCompleteEvent}
     * Отправляет на почту пользователя сообщение с ссылкой для подтверждения почты
     * Тут генерируется {@link Token} подтверждения почты
     */
    @Override
    public void onApplicationEvent(RegistrationCompleteEvent event) {
        User user = event.getUser();
        String domainName = applicationProperties.getDomainName();
        int port = applicationProperties.getPort();

        try {
            Token token = tokenService.generateToken(user, Token.Type.EMAIL_CONFIRM);
            String confirmUrl = "http://" + domainName + ":" + port + "/confirm?token=" + token.getStringRepresentation();
            String message = "Здравствуйте, " + user.getName()
                    + "!\nНажмите на ссылку ниже, чтобы подтвердить вашу электронную почту:\n" + confirmUrl;
            emailService.sendMessage(user.getEmail(), "Подтверждение почты", message);
        } catch (Exception exc) {
            log.error("Error sending email confirmation link to user: {}", user.getEmail(), exc);
        }
    }
}
