package ru.nsu.fit.db.railway.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.db.railway.entities.UserRole;

@Repository
public interface UserRolesRepository extends JpaRepository<UserRole, Integer> {
    UserRole findByRole(UserRole.Role role);
}