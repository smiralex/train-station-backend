package ru.nsu.fit.db.railway.schedule.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.db.railway.schedule.model.dto.TrainRideDto;
import ru.nsu.fit.db.railway.schedule.model.dto.WayPointEditDto;
import ru.nsu.fit.db.railway.schedule.mapper.TrainRideMapper;
import ru.nsu.fit.db.railway.schedule.services.TrainRideService;

import static ru.nsu.fit.db.railway.ApplicationProperties.PATH;

@Slf4j
@RestController
@RequestMapping(PATH + "/rides")
@RequiredArgsConstructor
public class TrainRideController {

    private final TrainRideService trainRideService;
    private final TrainRideMapper trainRideMapper;

    @GetMapping("/last-of-train/{trainId}")
    public TrainRideDto getLastRide(@PathVariable int trainId) {
        log.debug("Last train ride has queried");
        return trainRideMapper.toDto(trainRideService.getLastTrainRide(trainId));
    }

    @PostMapping("create")
    public void createTrainRide(@RequestBody TrainRideDto trainRideDto) {
        log.debug("Create train ride has queried");
        trainRideService.createTrainRide(trainRideDto);
    }

    @PostMapping("/waypoint/edit")
    public void editWayPoint(@RequestBody WayPointEditDto wayPointEditDto) {
        log.info("WayPoint edit has queried");
        trainRideService.editWayPoint(wayPointEditDto);
    }
}
