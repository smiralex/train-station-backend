package ru.nsu.fit.db.railway.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.db.railway.entities.trains.TrainStation;

import java.util.List;

@Repository
public interface StationsRepository extends JpaRepository<TrainStation, Integer > {

    @Query("SELECT DISTINCT st.stationCity FROM TrainStation st ")
    List<String> findAllCities();
}
