package ru.nsu.fit.db.railway.schedule.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.abstraction.mapper.Mapper;
import ru.nsu.fit.db.railway.entities.trains.TrainRide;
import ru.nsu.fit.db.railway.entities.trains.WayPoint;
import ru.nsu.fit.db.railway.schedule.model.dto.TrainRideDto;
import ru.nsu.fit.db.railway.schedule.model.dto.WayPointDto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
@RequiredArgsConstructor
public class TrainRideMapper implements Mapper<TrainRide, TrainRideDto> {

    private final ModelMapper modelMapper;

    @Override
    public TrainRide toEntity(TrainRideDto dto) {
        return null;
    }

    @Override
    public TrainRideDto toDto(TrainRide entity) {
        if (entity == null) {
            return null;
        }
        List<WayPoint> wayPoints = new ArrayList<>(entity.getWayPoints());
        List<WayPointDto> wayPointDtos = new ArrayList<>();
        wayPoints.stream()
                .sorted(Comparator.comparing(WayPoint::getArrivalDate))
                .forEach(wayPoint -> {
                    WayPointDto wpDto = modelMapper.map(wayPoint, WayPointDto.class);
                    wpDto.setStationId(wayPoint.getTrainStation().getId());
                    wpDto.setStationName(wayPoint.getTrainStation().getStationName());
                    wpDto.setStationCity(wayPoint.getTrainStation().getStationCity());
                    wayPointDtos.add(wpDto);
                });
        TrainRideDto trDto = modelMapper.map(entity, TrainRideDto.class);
        trDto.setRoute(wayPointDtos);
        return trDto;
    }
}
