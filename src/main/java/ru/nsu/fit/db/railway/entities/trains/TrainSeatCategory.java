package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "TrainSeatCategories")
public class TrainSeatCategory extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "TrainId")
    private Train train;

    @ManyToOne
    @JoinColumn(name = "SeatCategoryId")
    private SeatCategory seatCategory;

    private int numSeats;

    @OneToMany(mappedBy = "trainSeatCategory")
    private Set<Ticket> tickets;
}
