package ru.nsu.fit.db.railway.trains.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.db.railway.entities.trains.Train;
import ru.nsu.fit.db.railway.trains.model.TrainFilter;

import java.util.List;

@Repository
public interface TrainsRepository extends JpaRepository<Train, Integer> {

    @Query("SELECT DISTINCT tr.trainNumber FROM Train tr")
    List<Integer> findAllTrainNumbers();

    @Query(value = """
    SELECT DISTINCT arr.stationCity from TrainRide r 
    JOIN r.train t 
    JOIN t.arrivalStation arr 
    JOIN t.departureStation dep
    WHERE dep.stationCity = :cityFrom
""")
    List<String> findArrivalCities(@Param("cityFrom") String cityFrom);

    @Query(value = """
    SELECT DISTINCT dep.stationCity from TrainRide r 
    JOIN r.train t 
    JOIN t.arrivalStation arr 
    JOIN t.departureStation dep
    WHERE arr.stationCity = :cityTo
""")
    List<String> findDepartureCities(@Param("cityTo") String cityTo);

    @Query(value = """
    SELECT t FROM Train t 
    JOIN t.departureStation dep
    JOIN t.arrivalStation arr
    WHERE (:#{#filter.cityFrom} IS NULL OR :#{#filter.cityFrom} = dep.stationCity)
    AND (:#{#filter.cityTo} IS NULL OR :#{#filter.cityTo} = arr.stationCity)
    AND ((:#{#filter.trainNumbers}) IS NULL OR t.trainNumber IN (:#{#filter.trainNumbers}))
    """)
    Page<Train> findByFilter(@Param("filter") TrainFilter filter, Pageable pageable);

}
