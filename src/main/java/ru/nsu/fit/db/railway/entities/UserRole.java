package ru.nsu.fit.db.railway.entities;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;


@Entity
@Table(name = "Roles")
public class UserRole extends Identifiable implements GrantedAuthority {
    public enum Role {
        ADMIN,
        DEFAULT,
        UNCONFIRMED
    }

    @Enumerated(EnumType.STRING)
    private Role role;

    @Override
    public String getAuthority() {
        return role.toString();
    }
}
