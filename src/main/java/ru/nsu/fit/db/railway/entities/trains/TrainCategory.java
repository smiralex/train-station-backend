package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "TrainCategories")
public class TrainCategory extends Identifiable {

    private String categoryName;

    @OneToMany(mappedBy = "trainCategory")
    private Set<Train> trains;

}
