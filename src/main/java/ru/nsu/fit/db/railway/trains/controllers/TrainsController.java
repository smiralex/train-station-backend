package ru.nsu.fit.db.railway.trains.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.db.railway.abstraction.model.Filter;
import ru.nsu.fit.db.railway.abstraction.model.dto.PageableDto;
import ru.nsu.fit.db.railway.abstraction.mapper.MapperUtils;
import ru.nsu.fit.db.railway.common.dto.TrainStationDto;
import ru.nsu.fit.db.railway.entities.trains.Train;
import ru.nsu.fit.db.railway.trains.model.TrainFilter;
import ru.nsu.fit.db.railway.trains.mapper.TrainMapper;
import ru.nsu.fit.db.railway.trains.services.TrainsService;
import ru.nsu.fit.db.railway.trains.model.dto.TrainDto;

import java.util.List;

import static ru.nsu.fit.db.railway.ApplicationProperties.PATH;

@RestController
@RequestMapping(PATH + "/trains")
@RequiredArgsConstructor
@Slf4j
public class TrainsController {

    private final TrainsService trainsService;
    private final TrainMapper trainMapper;

    @GetMapping("/numbers/all")
    public List<Integer> getAllTrainIds() {
        log.debug("All trains ids has queried");
        return trainsService.getAllTrainIds();
    }

    @GetMapping("/seats/all")
    public List<String> getAllSeatCategories() {
        log.debug("All seat categories has been queried");
        return trainsService.getAllSeatCategories();
    }

    @PostMapping("/filter")
    public PageableDto<TrainDto> getFilteredTrains(@RequestBody Filter<TrainFilter> filter) {
        log.debug("All trains has queried");
        Page<Train> trains = trainsService.getFilteredTrains(filter);
        List<TrainDto> dtos = MapperUtils.entitiesToDtos(trains.getContent(), trainMapper);
        return new PageableDto<>(trains.getTotalElements(), trains.getTotalPages(), trains.getSize(), dtos);
    }

    @GetMapping("/route-by-train-id/{trainId}")
    public List<TrainStationDto> getRouteByTrainId(@PathVariable int trainId) {
        return trainsService.getRoute(trainId);
    }
}
