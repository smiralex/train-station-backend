package ru.nsu.fit.db.railway.schedule.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SeatsDto {

    private int numSeats;

    private int numPurchasedSeats;

}
