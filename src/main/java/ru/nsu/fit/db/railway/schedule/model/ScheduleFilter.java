package ru.nsu.fit.db.railway.schedule.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ScheduleFilter {

    private String cityFrom;

    private String cityTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date dateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date dateTo;

    private int minDelay;

    private int maxDelay;

    private List<Integer> trainNumbers;

    private List<String> seatTypes;

    private int numSeats;

}