package ru.nsu.fit.db.railway.entities;

import lombok.Getter;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.trains.TicketPurchase;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "Users")
public class User extends Identifiable {

    private String name;

    private String email;

    private String fullName;

    /**
     * В базе хранится в захешированном через {@link org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder} виде.
     */
    private String password;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "UserRoles",
            joinColumns = @JoinColumn(name = "UserId"),
            inverseJoinColumns = @JoinColumn(name = "RoleId")
    )
    private Set<UserRole> roles = new HashSet<>();

    @OneToMany(mappedBy = "customer")
    private Set<TicketPurchase> tickets;

}