package ru.nsu.fit.db.railway.common.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.db.railway.common.services.StationsService;

import java.util.List;

import static ru.nsu.fit.db.railway.ApplicationProperties.PATH;

@RestController
@RequestMapping(PATH + "/stations")
@RequiredArgsConstructor
@Slf4j
public class StationsController {

    private final StationsService stationsService;

    @GetMapping("/cities/all")
    public List<String> getAllCities(){
        log.debug("All cities has queried");
        return stationsService.getAllCities();
    }
}
