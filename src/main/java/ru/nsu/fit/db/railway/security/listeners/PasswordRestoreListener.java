package ru.nsu.fit.db.railway.security.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.ApplicationProperties;
import ru.nsu.fit.db.railway.entities.User;
import ru.nsu.fit.db.railway.security.events.PasswordRestoreEvent;
import ru.nsu.fit.db.railway.security.model.Token;
import ru.nsu.fit.db.railway.security.services.EmailService;
import ru.nsu.fit.db.railway.security.services.TokenService;


@Component
@RequiredArgsConstructor
@Slf4j
public class PasswordRestoreListener implements ApplicationListener<PasswordRestoreEvent> {

    private final ApplicationProperties applicationProperties;

    private final TokenService tokenService;

    private final EmailService emailService;

    /**
     * Этот метод вызывается, когда случился {@link PasswordRestoreEvent}
     * Отправляет на почту пользователя сообщение с ссылкой для восстановления пароля.
     * Тут генерируется {@link Token} восстановления пароля
     */

    @Override
    public void onApplicationEvent(PasswordRestoreEvent event) {
        User user = event.getUser();
        String domainName = applicationProperties.getDomainName();
        int port = applicationProperties.getPort();

        try {
            Token token = tokenService.generateToken(user, Token.Type.PASSWORD_RESTORE);
            String confirmUrl = "http://" + domainName + ":" + port + "/restore/" + token.getStringRepresentation();
            String message = "Здравствуйте, " + user.getName()
                    + "!\nНажмите на ссылку ниже, чтобы поменять пароль:\n" + confirmUrl;
            emailService.sendMessage(user.getEmail(), "Смена пароля", message);
        } catch (Exception exc) {
            log.error("Error sending password restore link to user: {}", user.getEmail(), exc);
        }
    }
}
