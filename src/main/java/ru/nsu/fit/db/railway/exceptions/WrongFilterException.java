package ru.nsu.fit.db.railway.exceptions;

public class WrongFilterException extends RuntimeException {
    public WrongFilterException(String message) {
        super(message);
    }
}
