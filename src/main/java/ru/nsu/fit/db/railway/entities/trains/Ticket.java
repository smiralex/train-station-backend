package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.*;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "Tickets")
public class Ticket extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "RailwayTrackId")
    private RailwayTrack railwayTrack;

    @ManyToOne
    @JoinColumn(name = "RideId")
    private TrainRide trainRide;

    @ManyToOne
    @JoinColumn(name = "TrainSeatCategoryId")
    private TrainSeatCategory trainSeatCategory;

    private int ticketCost;

}
