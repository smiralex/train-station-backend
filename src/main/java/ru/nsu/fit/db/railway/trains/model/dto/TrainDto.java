package ru.nsu.fit.db.railway.trains.model.dto;

import lombok.Getter;
import lombok.Setter;
import ru.nsu.fit.db.railway.common.dto.TrainStationDto;

import java.util.List;

@Getter
@Setter
public class TrainDto {

    private int id;

    private String departureCity;

    private String departureStationName;

    private String arrivalCity;

    private String arrivalStationName;

    private int trainNumber;

    private String trainCategory;

    private List<TrainStationDto> route;

}
