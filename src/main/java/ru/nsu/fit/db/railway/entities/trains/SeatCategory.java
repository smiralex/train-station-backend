package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "SeatCategories")
public class SeatCategory extends Identifiable {

    private String categoryName;

    @OneToMany(mappedBy = "seatCategory")
    private Set<TrainSeatCategory> trainSeatCategories;
}
