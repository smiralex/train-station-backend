package ru.nsu.fit.db.railway.schedule.model.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Builder
@EqualsAndHashCode(exclude = "seatsByCategory")
public class RouteEdgeDto {

    private WayPointDto depStation;

    private WayPointDto arrStation;

    @Setter
    private Map<String, SeatsDto> seatsByCategory;

}
