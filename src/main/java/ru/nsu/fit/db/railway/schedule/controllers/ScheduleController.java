package ru.nsu.fit.db.railway.schedule.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.db.railway.abstraction.model.Filter;
import ru.nsu.fit.db.railway.abstraction.model.dto.PageableDto;
import ru.nsu.fit.db.railway.exceptions.WrongFilterException;
import ru.nsu.fit.db.railway.schedule.model.ScheduleFilter;
import ru.nsu.fit.db.railway.schedule.model.dto.ScheduleRowDto;
import ru.nsu.fit.db.railway.schedule.services.ScheduleService;

import static ru.nsu.fit.db.railway.ApplicationProperties.PATH;

@Slf4j
@RestController
@RequestMapping(PATH + "/train-schedule")
@RequiredArgsConstructor
public class ScheduleController {

    private final ScheduleService scheduleService;

    @PostMapping("/filter")
    public PageableDto<ScheduleRowDto> getTrainRides(@RequestBody Filter<ScheduleFilter> filter) {
        log.info("Filtered schedule has queried ");
        if (filter.getPageNumber() < 0 || filter.getPageSize() < 0) {
            throw new WrongFilterException("PageNumber and pageSize have to be > 0");
        }
        return scheduleService.getSchedule(filter);
    }



}
