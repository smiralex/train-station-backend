package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "TrainEmployees")
public class TrainEmployee extends Identifiable {

    private String name;

    private String surname;

    private String Position;

    private String city;

    @ManyToMany(mappedBy = "crew")
    private Set<TrainRide> trainRides;
}

