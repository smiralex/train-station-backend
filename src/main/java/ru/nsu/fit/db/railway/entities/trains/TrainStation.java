package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "TrainStations")
public class TrainStation extends Identifiable {

    private String stationName;

    private String stationCity;

    @OneToMany(mappedBy = "trainStation")
    private Set<WayPoint> wayPoints;

    @OneToMany(mappedBy = "departureStation")
    private Set<RailwayTrack> railwayTracksAsDeparture;

    @OneToMany(mappedBy = "arrivalStation")
    private Set<RailwayTrack> railwayTracksAsArrival;

    @OneToMany(mappedBy = "headStation")
    private Set<Train> trainsAsHeads;

    @OneToMany(mappedBy = "departureStation")
    private Set<Train> trainsAsDeparture;

    @OneToMany(mappedBy = "arrivalStation")
    private Set<Train> trainsAsArrival;
}
