package ru.nsu.fit.db.railway.abstraction.mapper;

/**
 * Интерфейс, который маппит из Entity типа E в дто типа D
 */
public interface Mapper<E, D> {
    default E toEntity(D dto) {
        return null;
    }

    D toDto(E entity);
}