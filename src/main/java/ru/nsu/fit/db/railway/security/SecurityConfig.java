package ru.nsu.fit.db.railway.security;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import ru.nsu.fit.db.railway.security.jwt.JwtAuthenticationProvider;
import ru.nsu.fit.db.railway.security.jwt.JwtRequestFilter;
import ru.nsu.fit.db.railway.entities.UserRole.Role;
import static ru.nsu.fit.db.railway.ApplicationProperties.PATH;

/**
 * Общая конфигурация Spring Security
 */
@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthenticationProvider jwtAuthenticationProvider;

    private final JwtRequestFilter jwtRequestFilter;

    private final ExceptionHandlerFilter exceptionHandlerFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.httpBasic().disable();
        http.addFilterBefore(jwtRequestFilter, AbstractPreAuthenticatedProcessingFilter.class);
        http.addFilterBefore(exceptionHandlerFilter, JwtRequestFilter.class);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.anonymous()
                .and().authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(PATH + "/trains/**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(PATH + "/stations/**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(PATH + "/profile/**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(PATH + "/train-schedule/**").hasAuthority(Role.DEFAULT.name());

    }
}
