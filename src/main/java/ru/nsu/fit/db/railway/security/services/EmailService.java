package ru.nsu.fit.db.railway.security.services;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * Реализация почтового клиента, основанного на Spring Mail
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class EmailService {

    private final JavaMailSender emailSender;

    public void sendMessage(String[] to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
        log.info("Email message send to {}", StringUtils.join(to));
    }

    public void sendMessage(String to, String subject, String body) {
        sendMessage(new String[]{to}, subject, body);
    }
}