package ru.nsu.fit.db.railway.security.model;

import lombok.Data;

@Data
public class NewPasswordDto {
    //    @Pattern(regexp = RegexTemplateConstants.PASSWORD, message = "Некорректное поле 'Новый пароль'")
    private String newPassword;
}
