package ru.nsu.fit.db.railway.common.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.abstraction.mapper.Mapper;
import ru.nsu.fit.db.railway.common.dto.UserDto;
import ru.nsu.fit.db.railway.entities.User;


@Component
@RequiredArgsConstructor
public class UserMapper implements Mapper<User, UserDto> {
    private final ModelMapper modelMapper;

    @Override
    public User toEntity(UserDto dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public UserDto toDto(User entity) {
        return modelMapper.map(entity, UserDto.class);
    }
}
