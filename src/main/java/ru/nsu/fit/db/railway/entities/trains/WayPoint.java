package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "WayPoints")
public class WayPoint extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "StationId")
    private TrainStation trainStation;

    @ManyToOne
    @JoinColumn(name = "RideId")
    private TrainRide trainRide;

    @Temporal(TemporalType.TIMESTAMP)
    private Date arrivalDate;

    private int stayTime;

    private int delay;
}
