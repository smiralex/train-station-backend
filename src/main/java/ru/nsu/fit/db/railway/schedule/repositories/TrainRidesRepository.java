package ru.nsu.fit.db.railway.schedule.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.db.railway.entities.trains.TrainRide;

@Repository
public interface TrainRidesRepository extends JpaRepository<TrainRide, Integer>, ScheduleRepository {

    @Query("""
    SELECT tr FROM TrainRide tr JOIN FETCH tr.wayPoints wp JOIN FETCH wp.trainStation WHERE tr.id = :id
""")
    TrainRide getWithWayPointsById(@Param("id") int id);

    @Query(value = """
    SELECT * FROM TrainRides tr 
    JOIN WayPoints wp ON tr.id = wp.rideid
    JOIN TrainStations ts ON wp.stationid = ts.id
    WHERE tr.trainid = :trainId
    ORDER BY tr.arrivalDate DESC
    LIMIT 1
""", nativeQuery =true)
    TrainRide findLastTrainRide(@Param("trainId") int trainId);

}
