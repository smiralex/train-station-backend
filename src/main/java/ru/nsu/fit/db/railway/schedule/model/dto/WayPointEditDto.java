package ru.nsu.fit.db.railway.schedule.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class WayPointEditDto {

    private int rideId;

    private int stationId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
    private Date arrivalDate;

    private Integer stayTime;

    private Integer delay;

}