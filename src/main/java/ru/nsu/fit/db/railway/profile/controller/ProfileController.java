package ru.nsu.fit.db.railway.profile.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.db.railway.profile.dto.ProfileDto;
import ru.nsu.fit.db.railway.profile.mapper.ProfileMapper;
import ru.nsu.fit.db.railway.common.services.UsersService;

import static ru.nsu.fit.db.railway.ApplicationProperties.PATH;

@RestController
@RequiredArgsConstructor
@RequestMapping(PATH)
public class ProfileController {

    private final UsersService usersService;

    private final ProfileMapper profileMapper;

    @GetMapping("/profile")
    public ProfileDto getProfile() {
        return profileMapper.toDto(usersService.getCurrentUser());
    }
}
