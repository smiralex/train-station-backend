package ru.nsu.fit.db.railway.schedule.mapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.schedule.model.dto.RouteEdgeDto;
import ru.nsu.fit.db.railway.schedule.model.dto.SeatsDto;
import ru.nsu.fit.db.railway.schedule.model.dto.WayPointDto;
import ru.nsu.fit.db.railway.schedule.model.ScheduleRow;
import ru.nsu.fit.db.railway.schedule.model.dto.ScheduleRowDto;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Slf4j
public class ScheduleMapper {

    public Collection<ScheduleRowDto> toDto(List<ScheduleRow> scheduleRows) {
        Map<Integer, ScheduleRowDto> result = new HashMap<>();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ScheduleRowDto scheduleRowDto;
        for (ScheduleRow scheduleRow: scheduleRows) {
            try {
                WayPointDto depStation = WayPointDto.builder()
                        .stationId(scheduleRow.getDepStId())
                        .stationCity(scheduleRow.getDepStCity())
                        .stationName(scheduleRow.getDepStName())
                        .arrivalDate(dateFormat.parse(scheduleRow.getDepDate()))
                        .delay(scheduleRow.getDepDelay())
                        .stayTime(scheduleRow.getDepStayTime())
                        .build();
                WayPointDto arrStation = WayPointDto.builder()
                        .stationId(scheduleRow.getArrStId())
                        .stationCity(scheduleRow.getArrStCity())
                        .stationName(scheduleRow.getArrStName())
                        .arrivalDate(dateFormat.parse(scheduleRow.getArrDate()))
                        .delay(scheduleRow.getArrDelay())
                        .stayTime(scheduleRow.getArrStayTime())
                        .build();
                RouteEdgeDto routeEdgeDto = RouteEdgeDto.builder()
                        .depStation(depStation)
                        .arrStation(arrStation)
                        .build();

                if (result.containsKey(scheduleRow.getRideId())) {
                    scheduleRowDto = result.get(scheduleRow.getRideId());
                } else {
                    scheduleRowDto = new ScheduleRowDto();
                    scheduleRowDto.setRoute(new ArrayList<>());
                    scheduleRowDto.setTrainNumber(scheduleRow.getTrainNumber());
                    scheduleRowDto.setTrainDepStCity(scheduleRow.getTrainDepStCity());
                    scheduleRowDto.setTrainArrStCity(scheduleRow.getTrainArrStCity());
                    scheduleRowDto.setRideId(scheduleRow.getRideId());
                    scheduleRowDto.setRideDepDate(scheduleRow.getRideDepDate());
                    scheduleRowDto.setRideArrDate(scheduleRow.getRideArrDate());
                    result.put(scheduleRow.getRideId(), scheduleRowDto);
                }
                if (scheduleRowDto.getRoute().contains(routeEdgeDto)) {
                    int index = scheduleRowDto.getRoute().indexOf(routeEdgeDto);
                    routeEdgeDto = scheduleRowDto.getRoute().get(index);
                } else {
                    scheduleRowDto.getRoute().add(routeEdgeDto);
                    routeEdgeDto.setSeatsByCategory(new HashMap<>());
                }
                routeEdgeDto.getSeatsByCategory().put(scheduleRow.getSeatCategory(),
                        new SeatsDto(scheduleRow.getNumSeats(), scheduleRow.getNumPurchasedSeats()));
            } catch (ParseException e) {
                log.error("Failed to parse date from ScheduleRow: {}", scheduleRow.getArrDate());
            }

        }
        for (ScheduleRowDto scheduleObj: result.values()) {
            scheduleObj.getRoute().sort(Comparator.comparing(route -> route.getArrStation().getArrivalDate()));
        }
        return result.values();
    }
}