package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;
import ru.nsu.fit.db.railway.entities.User;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TicketsPurchases")
public class TicketPurchase extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "UserId")
    private User customer;

    @OneToOne
    @JoinColumn(name = "TicketId")
    private Ticket ticket;

    @Temporal(TemporalType.TIMESTAMP)
    private Date purchaseDate;
}
