package ru.nsu.fit.db.railway.profile.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileDto {

    private String name;

    private String email;

    private String fullName;

}