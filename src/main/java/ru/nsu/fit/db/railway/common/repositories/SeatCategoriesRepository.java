package ru.nsu.fit.db.railway.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.db.railway.entities.trains.SeatCategory;

import java.util.List;

@Repository
public interface SeatCategoriesRepository extends JpaRepository<SeatCategory, Integer> {

    @Query("SELECT DISTINCT seat.categoryName FROM SeatCategory seat")
    List<String> findAllNames();
}
