package ru.nsu.fit.db.railway.security.events;

import org.springframework.context.ApplicationEvent;
import ru.nsu.fit.db.railway.entities.User;


/**
 * Событие создается, когда пользователь хочет восстановить пароль
 */
public class PasswordRestoreEvent extends ApplicationEvent {

    public PasswordRestoreEvent(User user) {
        super(user);
    }

    public User getUser() {
        return (User) super.source;
    }
}
