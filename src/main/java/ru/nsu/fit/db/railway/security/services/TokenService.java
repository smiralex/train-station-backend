package ru.nsu.fit.db.railway.security.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.db.railway.entities.User;
import ru.nsu.fit.db.railway.common.repositories.TokensRepository;
import ru.nsu.fit.db.railway.security.model.Token;

import java.time.Clock;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

/**
 * Сервис для манипуляции токенами восстановления пароля и подтверждения почты
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TokenService {
    private final TokensRepository repository;

    private final Clock clock;

    @DurationUnit(ChronoUnit.DAYS)
    @Value("${token.expiration}")
    private Duration duration;

    public User getUser(String stringToken, Token.Type type) {
        Token token = getTokenByStringAndType(stringToken, type);
        return token.getUser();
    }

    public boolean isTokenExpired(Token token) {
        return token.getExpirationDate().before(new Date(clock.millis()));
    }

    public Token validateToken(String stringToken, Token.Type type) {
        Token token = getTokenByStringAndType(stringToken, type);
        if (isTokenExpired(token)) {
            throw new IllegalArgumentException("Wrong token");
        }

        return token;
    }

    public void removeToken(int id) {
        repository.deleteById(id);
    }

    @Transactional
    public Token generateToken(User user, Token.Type type) {
        Token token = new Token();
        Date expirationDate = new Date(clock.millis() + duration.toMillis());
        String stringToken = UUID.randomUUID().toString();
        token.setUser(user);
        token.setExpirationDate(expirationDate);
        token.setStringRepresentation(stringToken);
        token.setType(type);

        log.info("Created token for use {}. Type of token : {}", user.getName(), type);
        return repository.save(token);
    }

    protected Token getTokenByStringAndType(String stringToken, Token.Type type) {
        return repository
                .findByStringRepresentationAndType(stringToken, type)
                .orElseThrow(() -> new IllegalArgumentException("Wrong token"));
    }
}
