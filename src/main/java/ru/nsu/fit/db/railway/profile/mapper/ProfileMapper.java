package ru.nsu.fit.db.railway.profile.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.abstraction.mapper.Mapper;
import ru.nsu.fit.db.railway.profile.dto.ProfileDto;
import ru.nsu.fit.db.railway.entities.User;

@Component
@RequiredArgsConstructor
public class ProfileMapper implements Mapper<User, ProfileDto> {

    private final ModelMapper modelMapper;

    @Override
    public ProfileDto toDto(User entity) {
        return modelMapper.map(entity, ProfileDto.class);
    }
}