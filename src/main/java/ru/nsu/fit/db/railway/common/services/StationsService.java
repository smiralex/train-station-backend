package ru.nsu.fit.db.railway.common.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.nsu.fit.db.railway.common.repositories.StationsRepository;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class StationsService {

    private final StationsRepository stationsRepository;

    public List<String> getAllCities() {
        return stationsRepository.findAllCities();
    }
}
