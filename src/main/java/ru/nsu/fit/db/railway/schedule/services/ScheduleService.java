package ru.nsu.fit.db.railway.schedule.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.fit.db.railway.abstraction.model.Filter;
import ru.nsu.fit.db.railway.abstraction.model.dto.PageableDto;
import ru.nsu.fit.db.railway.trains.repositories.TrainsRepository;
import ru.nsu.fit.db.railway.entities.trains.Train;
import ru.nsu.fit.db.railway.schedule.model.ScheduleFilter;
import ru.nsu.fit.db.railway.schedule.model.ScheduleRow;
import ru.nsu.fit.db.railway.schedule.model.dto.ScheduleRowDto;
import ru.nsu.fit.db.railway.schedule.mapper.ScheduleMapper;
import ru.nsu.fit.db.railway.schedule.repositories.TrainRidesRepository;

import java.util.*;

@Service
@RequiredArgsConstructor
public class ScheduleService {

    private final TrainRidesRepository trainRidesRepository;
    private final TrainsRepository trainsRepository;
    private final ScheduleMapper scheduleMapper;

    public PageableDto<ScheduleRowDto> getSchedule(Filter<ScheduleFilter> filter) {
        ScheduleFilter scheduleFilter = filter.getFilter();
        List<ScheduleRowDto> scheduleRowDtos = new ArrayList<>();
        if (scheduleFilter.getCityFrom() == null && scheduleFilter.getCityTo() == null) {
            List<Train> trains = trainsRepository.findAll();
            for (Train train: trains) {
                scheduleFilter.setCityFrom(train.getDepartureStation().getStationCity());
                scheduleFilter.setCityTo(train.getArrivalStation().getStationCity());
                scheduleRowDtos.addAll(scheduleMapper.toDto(trainRidesRepository.findSchedule(scheduleFilter)));
            }
        } else if (scheduleFilter.getCityFrom() == null && scheduleFilter.getCityTo() != null) {
            List<String> departureCities = trainsRepository.findDepartureCities(scheduleFilter.getCityTo());
            for (String cityFrom: departureCities) {
                scheduleFilter.setCityFrom(cityFrom);
                List<ScheduleRow> scheduleRows = trainRidesRepository.findSchedule(scheduleFilter);
                scheduleRowDtos.addAll(scheduleMapper.toDto(scheduleRows));
            }
        } else if (scheduleFilter.getCityFrom() != null && scheduleFilter.getCityTo() == null) {
            List<String> arrivalCities = trainsRepository.findArrivalCities(scheduleFilter.getCityFrom());
            for (String cityTo: arrivalCities) {
                scheduleFilter.setCityTo(cityTo);
                List<ScheduleRow> scheduleRows = trainRidesRepository.findSchedule(scheduleFilter);
                scheduleRowDtos.addAll(scheduleMapper.toDto(scheduleRows));
            }
        } else {
            scheduleRowDtos.addAll(scheduleMapper.toDto(trainRidesRepository.findSchedule(scheduleFilter)));
        }
        if (filter.getPageSize() == 0) {
            filter.setPageSize(PageableDto.DEFAULT_PAGE_SIZE);
        }
        int indexFrom = filter.getPageNumber() * filter.getPageSize();
        int indexTo = indexFrom + filter.getPageSize();
        int numPages = scheduleRowDtos.size() / filter.getPageSize();
        if (scheduleRowDtos.size() % filter.getPageSize() != 0) {
            numPages++;
        }
        if (indexTo < scheduleRowDtos.size()) {
            return new PageableDto<>(scheduleRowDtos.size(), numPages,  filter.getPageSize(),
                    scheduleRowDtos.subList(indexFrom, indexTo));
        } else if (indexFrom < scheduleRowDtos.size()) {
            return new PageableDto<>(scheduleRowDtos.size(), numPages, filter.getPageSize(),
                    scheduleRowDtos.subList(indexFrom, scheduleRowDtos.size()));
        }
        return new PageableDto<>(scheduleRowDtos.size(), numPages, filter.getPageSize(),
                new ArrayList<>());
    }
}
