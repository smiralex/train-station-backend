package ru.nsu.fit.db.railway.common.dto;

import lombok.Getter;

@Getter
public class UserDto {

    private int id;

    private String name;

    private String email;

    private String fullName;

    private String password;
}
