package ru.nsu.fit.db.railway.abstraction.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;

@Getter
@Setter
public class Filter<T> {

    private int pageSize;

    private int pageNumber;

    @NonNull
    private T filter;
}
