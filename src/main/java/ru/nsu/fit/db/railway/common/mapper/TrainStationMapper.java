package ru.nsu.fit.db.railway.common.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.fit.db.railway.abstraction.mapper.Mapper;
import ru.nsu.fit.db.railway.common.dto.TrainStationDto;
import ru.nsu.fit.db.railway.entities.trains.TrainStation;

@Component
@RequiredArgsConstructor
public class TrainStationMapper implements Mapper<TrainStation, TrainStationDto> {

    private final ModelMapper modelMapper;

    @Override
    public TrainStation toEntity(TrainStationDto dto) {
        return modelMapper.map(dto, TrainStation.class);
    }

    @Override
    public TrainStationDto toDto(TrainStation entity) {
        return modelMapper.map(entity, TrainStationDto.class);
    }
}
