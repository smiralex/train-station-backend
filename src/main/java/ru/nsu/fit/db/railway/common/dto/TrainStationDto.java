package ru.nsu.fit.db.railway.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainStationDto {

    @JsonProperty("stationId")
    private int id;

    private String stationName;

    private String stationCity;

}