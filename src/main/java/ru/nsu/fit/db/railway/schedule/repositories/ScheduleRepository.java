package ru.nsu.fit.db.railway.schedule.repositories;

import ru.nsu.fit.db.railway.schedule.model.ScheduleFilter;
import ru.nsu.fit.db.railway.schedule.model.ScheduleRow;

import java.util.List;

public interface ScheduleRepository {

    List<ScheduleRow> findSchedule(ScheduleFilter scheduleFilter);
}
