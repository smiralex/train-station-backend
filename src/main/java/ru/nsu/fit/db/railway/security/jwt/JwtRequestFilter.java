package ru.nsu.fit.db.railway.security.jwt;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Этот класс просто вытаскивает из http-запроса хедер с токеном и добавляет его в контекст, чтобы дальше
 * {@link JwtAuthenticationProvider} смог понять истек токен или нет.
 * Если токена нет в заголовке, то это равнозначно тому, что если бы {@link JwtAuthenticationProvider} вернул null
 * то есть пользователь неавторизирован
 */

@Component
@RequiredArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {
    private final JwtProvider jwtProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String stringJwt = jwtProvider.getTokenFromRequest(request);
        if (stringJwt != null) {
            SecurityContextHolder.getContext().setAuthentication(new JwtAuthentication(stringJwt));
        }

        filterChain.doFilter(request, response);
    }
}
