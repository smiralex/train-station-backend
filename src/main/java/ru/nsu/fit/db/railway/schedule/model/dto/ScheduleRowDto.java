package ru.nsu.fit.db.railway.schedule.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ScheduleRowDto {

    private String trainDepStCity;

    private String trainArrStCity;

    private int trainNumber;

    private int rideId;

    private String rideDepDate;

    private String rideArrDate;

    private List<RouteEdgeDto> route;

}
