package ru.nsu.fit.db.railway.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.db.railway.entities.User;
import ru.nsu.fit.db.railway.security.model.Token;


import java.util.Optional;

@Repository
public interface TokensRepository extends JpaRepository<Token, Integer> {
    void deleteByUserAndType(User user, Token.Type type);

    Optional<Token> findByStringRepresentationAndType(String stringRepresentation, Token.Type type);
}
