package ru.nsu.fit.db.railway.abstraction.mapper;

import java.util.List;
import java.util.stream.Collectors;

public class MapperUtils {
    public static <E, D> List<D> entitiesToDtos(List<E> entities, Mapper<? super E, D> mapper) {
        return entities.stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
