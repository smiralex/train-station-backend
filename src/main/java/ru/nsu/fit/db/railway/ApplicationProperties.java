package ru.nsu.fit.db.railway;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
    public static final String PATH = "/api/v1";

    private String domainName;

    private int port;
}
