package ru.nsu.fit.db.railway.abstraction.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class PageableDto<T> {

    public static final int DEFAULT_PAGE_SIZE = 5;

    private long numTotalElements;

    private int numPages;

    private int pageSize;

    private List<T> elements;

}
