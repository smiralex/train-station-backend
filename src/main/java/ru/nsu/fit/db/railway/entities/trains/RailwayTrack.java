package ru.nsu.fit.db.railway.entities.trains;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.db.railway.entities.Identifiable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "RailwayTracks")
public class RailwayTrack extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "DepartureStationId")
    private TrainStation departureStation;

    @ManyToOne
    @JoinColumn(name = "ArrivalStationId")
    private TrainStation arrivalStation;

    private int distance;

    @ManyToMany(mappedBy = "railwayTracks")
    private Set<Train> trains;

    @OneToMany(mappedBy = "railwayTrack")
    private Set<Ticket> tickets;
}
