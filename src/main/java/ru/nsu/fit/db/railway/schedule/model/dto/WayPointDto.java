package ru.nsu.fit.db.railway.schedule.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"stationId", "arrivalDate"})
public class WayPointDto {

    private int stationId;

    private String stationName;

    private String stationCity;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm")
    private Date arrivalDate;

    private int stayTime;

    private int delay;

}
