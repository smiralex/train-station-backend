package ru.nsu.fit.db.railway.common.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.fit.db.railway.exceptions.NotFoundException;
import ru.nsu.fit.db.railway.entities.User;
import ru.nsu.fit.db.railway.common.repositories.UsersRepository;

@Service
@RequiredArgsConstructor
public class UsersService {

    private final UsersRepository userRepository;

    public User getUserByNickname(String nickname) {
        return userRepository
                .findByNameIgnoreCase(nickname)
                .orElseThrow(() -> new IllegalArgumentException("Wrong nickname"));
    }

    public User getUserByEmail(String email) {
        return userRepository
                .findByEmailIgnoreCase(email)
                .orElseThrow(() -> new IllegalArgumentException("Wrong email"));
    }

    public User getCurrentUser() {
        return userRepository
                .findCurrentUser()
                .orElseThrow(() -> new IllegalArgumentException("Unauthorized"));
    }

    public User getById(int id) {
        return userRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("User not found"));
    }
}
