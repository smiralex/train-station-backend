package ru.nsu.fit.db.railway.trains.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TrainFilter {

    private String cityFrom;

    private String cityTo;

    private List<Integer> trainNumbers;
}
