INSERT INTO seatcategories (id, categoryname) VALUES
(1, 'Общий'), (2, 'Плацкартный'), (3, 'Купе'), (4, 'Свободный вагон');
SELECT setval('seatcategories_id_seq', 4, true);

INSERT INTO traincategories (id, categoryname) VALUES
(1, 'Пассажирский'), (2, 'Скоростной'), (3, 'Грузовой');
SELECT setval('traincategories_id_seq', 3, true);

INSERT INTO trainstations (id, stationname, stationcity) VALUES
(1, 'Имени Ленина', 'Москва'), (2, 'Сибирская', 'Новосибирск'), (3, 'Шахтеров', 'Кемерово'),
(4, 'Забайкальская', 'Владивосток'), (5, 'Петра Великого', 'Санкт-Петербург'), (6, 'Имени Путина', 'Москва'),
(7, 'Горная', 'Екатеринбург'), (8, 'Вологодская', 'Нижний Новгород'), (9, 'Солнечная', 'Грозный'),
(10, 'Кузнецкая', 'Новокузнецк');
SELECT setval('trainstations_id_seq', 10, true);

INSERT INTO trains (id, trainnumber, traincategoryid, departurestationid, arrivalstationid, headstationid) VALUES
(1, 1111, 2, 6, 5, 6), (2, 1111, 2, 5, 6, 6), (3, 3333, 2, 6, 1, 1), (4, 3333, 2, 1, 6, 1), (5, 5555, 1, 1, 4, 1),
(6, 5555, 1, 4, 1, 1), (7, 7777, 1, 2, 3, 2), (8, 7777, 1, 3, 2, 3), (9, 9999, 1, 1, 9, 9), (10, 9999, 1, 9, 1, 9);
SELECT setval('trains_id_seq', 10, true);


INSERT INTO railwaytracks (id, departurestationid, arrivalstationid, distance) VALUES
(1, 6, 5, 2), (2, 5 ,6, 2), (3, 6, 1, 1), (4, 1, 6, 1), (5, 1, 8, 4), (6, 8, 1, 4), (7, 8, 7, 4), (8, 7, 8, 4),
(9, 7, 2, 5), (10, 2, 7, 5), (11, 2, 10 ,3), (12, 10, 2, 3), (13, 10, 3, 2), (14, 3, 10, 2), (15, 2, 4, 8),
(16, 4, 2, 8), (17, 1, 9, 7), (18, 9, 1, 7);
SELECT setval('railwaytracks_id_seq', 18, true);

INSERT INTO routeedges (railwaytrackid, trainid) VALUES
(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 5), (8, 6), (9, 5), (10, 6), (11, 7), (12, 8), (13, 7), (14, 8),
(15, 5), (16, 6), (17, 9), (18, 10);

INSERT INTO trainseatcategories (id, trainid, seatcategoryid, numseats) VALUES
(2, 5, 2, 20), (4, 5, 4, 5), (1, 5, 1, 10), (3, 5, 3, 30), (5, 5, 2, 20), (6, 6, 4, 5), (7, 6, 1, 10), (8, 6, 3, 30);
SELECT setval('trainseatcategories_id_seq', 8, true);

INSERT INTO trainrides (id, trainid, departuredate, arrivaldate) VALUES
(1, 1, to_timestamp('01.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('02.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS')),
(2, 2, to_timestamp('02.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('03.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS')),
(3, 1, to_timestamp('03.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('04.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS')),
(4, 2, to_timestamp('04.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('05.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS')),
(5, 3, to_timestamp('04.11.2019 12:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('04.11.2019 14:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(6, 4, to_timestamp('04.11.2019 14:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('04.11.2019 16:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(7, 3, to_timestamp('04.11.2019 12:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('04.11.2019 14:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(8, 4, to_timestamp('04.11.2019 16:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('04.11.2019 18:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(9, 5, to_timestamp('01.11.2019 12:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('05.11.2019 14:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(10, 6, to_timestamp('05.11.2019 16:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('09.11.2019 18:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(11, 5, to_timestamp('06.11.2019 12:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('10.11.2019 14:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(12, 6, to_timestamp('10.11.2019 16:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('15.11.2019 18:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(13, 7, to_timestamp('03.11.2019 12:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('03.11.2019 18:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(14, 8, to_timestamp('04.11.2019 19:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('05.11.2019 01:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(15, 7, to_timestamp('06.11.2019 19:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('07.11.2019 01:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(16, 8, to_timestamp('07.11.2019 19:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('08.11.2019 01:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(17, 9, to_timestamp('06.11.2019 19:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('08.11.2019 01:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(18, 10, to_timestamp('07.11.2019 19:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('09.11.2019 01:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(19, 9, to_timestamp('06.11.2019 19:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('08.11.2019 01:00:00', 'DD.MM.YYYY HH24:MI:SS')),
(20, 10, to_timestamp('08.11.2019 19:00:00', 'DD.MM.YYYY HH24:MI:SS'), to_timestamp('10.11.2019 01:00:00', 'DD.MM.YYYY HH24:MI:SS'));
SELECT setval('trainrides_id_seq', 20, true);

INSERT INTO waypoints (id, stationid, rideid, arrivaldate, staytime, delay) VALUES
(1, 1, 9, to_timestamp('01.11.2019 12:00:00', 'DD.MM.YYYY HH24:MI:SS'), 0, 0),
(2, 8, 9, to_timestamp('02.11.2019 23:30:00', 'DD.MM.YYYY HH24:MI:SS'), 15, 5),
(3, 7, 9, to_timestamp('03.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), 35, 10),
(4, 2, 9, to_timestamp('04.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), 15, 13),
(5, 4, 9, to_timestamp('05.11.2019 14:00:00', 'DD.MM.YYYY HH24:MI:SS'), 0, 13),
(6, 4, 10, to_timestamp('05.11.2019 16:00:00', 'DD.MM.YYYY HH24:MI:SS'), 0, 0),
(7, 2, 10, to_timestamp('06.11.2019 23:30:00', 'DD.MM.YYYY HH24:MI:SS'), 15, 2),
(8, 7, 10, to_timestamp('07.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), 35, 4),
(9, 8, 10, to_timestamp('08.11.2019 18:30:00', 'DD.MM.YYYY HH24:MI:SS'), 15, 4),
(10, 1, 10, to_timestamp('09.11.2019 18:00:00', 'DD.MM.YYYY HH24:MI:SS'), 0, 10),
(11, 2, 13, to_timestamp('03.11.2019 12:00:00', 'DD.MM.YYYY HH24:MI:SS'), 0, 0),
(12, 10, 13, to_timestamp('03.11.2019 15:30:00', 'DD.MM.YYYY HH24:MI:SS'), 15, 15),
(13, 3, 13, to_timestamp('03.11.2019 18:00:00', 'DD.MM.YYYY HH24:MI:SS'), 0, 15);
SELECT setval('waypoints_id_seq', 13, true);

INSERT INTO Tickets (railwaytrackid, rideid, trainseatcategoryid, ticketcost) VALUES
(9, 9, 1, 6), (9, 9, 2, 19), (9, 9, 3, 27), (15, 9, 1, 8), (15, 9 , 2, 16), (5, 9, 3, 20), (7, 9, 1, 4), (11, 13, 1, 19),
(14, 16, 1, 5), (7, 9, 2, 15), (5, 9, 4, 4), (7, 9, 4, 4), (9, 9, 4, 5), (15, 9, 4, 1), (13, 13, 2, 6), (11, 15, 1, 10),
(13, 15, 2, 5), (14, 14, 2, 3), (12, 16, 1, 5), (12, 14, 3, 1), (5, 9, 1, 23), (5, 9, 2, 98);

INSERT INTO trainemployees (id, name, surname, position, city) VALUES
(1, 'Иван', 'Иванов', 'Проводник', 'Москва'), (2, 'Мухамед', 'Алиев', 'Машинист', 'Грозный'),
(3, 'Александр', 'Смирнов', 'Машинист', 'Новосибирск');
SELECT setval('trainemployees_id_seq', 3, true);

INSERT INTO traincrews (rideid, employeeid) VALUES
(1, 2), (2, 2), (13, 1), (14, 1);