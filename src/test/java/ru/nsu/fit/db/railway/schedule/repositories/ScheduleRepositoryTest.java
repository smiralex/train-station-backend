package ru.nsu.fit.db.railway.schedule.repositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.nsu.fit.db.railway.schedule.model.ScheduleFilter;
import ru.nsu.fit.db.railway.schedule.model.ScheduleRow;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
class ScheduleRepositoryTest {

    @Autowired
    private TrainRidesRepository trainRidesRepository;

    @Test
    void findSchedule() throws Exception {
        String dateFrom = "01-11-2019";
        String dateTo = "05-11-2019";
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        ScheduleFilter scheduleFilter = new ScheduleFilter();
        scheduleFilter.setCityFrom("Москва");
        scheduleFilter.setCityTo("Екатеринбург");
        scheduleFilter.setMaxDelay(-1);
        scheduleFilter.setDateFrom(format.parse(dateFrom));
        scheduleFilter.setDateTo(format.parse(dateTo));
//        scheduleFilter.setSeatTypes(List.of("Плацкартный"));
        List<ScheduleRow> rows = trainRidesRepository.findSchedule(scheduleFilter);
        System.out.println(rows.size());
        rows.forEach(row -> System.out.println(row.toString()));
    }
}